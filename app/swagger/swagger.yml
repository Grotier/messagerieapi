swagger: "2.0"
info:
  description: "Service de messagerie avec groupe de discussions créé dans le cadre de l'UE Réseaux et Applications réparties de la filière Réseaux et Systèmes d'Information de l'ENSEIRB-MATMECA."
  version: "1.0.0"
  title: "Service de messagerie Bas&Go"
  contact:
    email: "bduchesne001@enseirb-matmeca.fr"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "localhost:5002"
basePath: ""
tags:
- name: "Users"
  description: "Utilisateurs de la messagerie"
- name: "Channels"
  description: "Groupes de discussion de la messagerie"
- name: "Messages"
  description: "Messages envoyés"
schemes:
- "http"
paths:
  /users:
    post:
      tags:
      - "Users"
      summary: "Crée un nouvel utilisateur"
      description: ""
      operationId: "addUser"
      consumes:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "L'objet \"user\" à ajouter dans la base."
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        200:
          description: "Success"
        400:
          description: "Bad request from client"
        500:
          description: "Error from server"
    get:
      tags:
      - "Users"
      summary: "Renvoie la liste des utilisateurs de la base."
      description: ""
      operationId: "getUsers"
      produces:
      - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
            $ref: "#/definitions/User"
        500:
          description: "Error from server"
  /users/{userName}:
    delete:
      tags:
      - "Users"
      summary: "Supprime un utilisateur de la base."
      description: ""
      parameters:
      - in: path
        name: userName
        type: "string"
        required: true
        description: Nom de l'utilisateur à supprimer.
      operationId: "deleteUser"
      responses:
        200:
          description: "Success"
        404:
          description: "Not found"
        500:
          description: "Error from server"
  /channels:
    post:
      tags:
      -  "Channels"
      summary: "Crée un nouveau groupe de discussion."
      description: ""
      operationId: "addChannel"
      consumes:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "L'objet \"channel\" à ajouter dans la base."
        required: true
        schema:
          $ref: "#/definitions/Channel"
      responses:
        200:
          description: "Success"
        400:
          description: "Bad request from client"
        500:
          description: "Error from server"
    get:
      tags:
      - "Channels"
      summary: "Renvoie la liste des groupes de discussion de la base."
      description: ""
      operationId: "getChannels"
      produces:
      - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
            $ref: "#/definitions/Channel"
        500:
          description: "Error from server"
    
  /channels/{channelName}:
    delete:
      tags:
      - "Channels"
      summary: "Supprime un groupe de discussion de la base. Supprime aussi tous les messages du groupe."
      description: ""
      parameters:
      - in: path
        name: channelName
        type: "string"
        required: true
        description: Le nom du groupe de discussion à supprimer.
      operationId: "deleteChannel"
      responses:
        200:
          description: "Success"
        404:
          description: "Not found"
        500:
          description: "Error from server"
    
  /messages/{userName}:
    post:
      tags:
      - "Messages"
      summary: "Envoie un message à un utilisateur."
      description: ""
      operationId: "sendMessageToUser"
      consumes:
      - "application/json"
      parameters:
      - in: path
        name: userName
        type: "string"
        required: true
        description: Nom de l'utilisateur à qui on veut envoyer un message.
      - in: "body"
        name: "body"
        description: "L'objet \"message\" à ajouter dans la base."
        required: true
        schema:
          $ref: "#/definitions/Message"
      responses:
        200:
          description: "Success"
        400:
          description: "Bad request"
        404:
          description: "User not found"
        500:
          description: "Error from server"
    get:
      tags:
      - "Messages"
      summary: "Renvoie les messages adressés à un utilisateur."
      description: ""
      operationId: "getUserMessages"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: path
        name: userName
        type: "string"
        required: true
        description: Nom de l'utilisateur dont on veut voir les messages.
        schema:
          $ref: "#/definitions/User"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
            $ref: "#/definitions/Message"
        404:
          description: "Not found"
        500:
          description: "Error from server"
  /messages/channel/{channelName}:
    post:
      tags:
      - "Messages"
      summary: "Envoie un message à un groupe de discussion."
      description: ""
      operationId: "sendMessageToChannel"
      consumes:
      - "application/json"
      parameters:
      - in: path
        name: channelName
        type: "string"
        required: true
        description: Nom du groupe de discussion où l'on veut poster un message.
      - in: "body"
        name: "body"
        description: "L'objet \"message\" à ajouter dans la base."
        required: true
        schema:
          $ref: "#/definitions/Message"
      responses:
        200:
          description: "Success"
        400:
          description: "Bad request"
        404:
          description: "Channel/user not found"
        500:
          description: "Error from server"
    get:
      tags:
      - "Messages"
      summary: "Renvoie les messages d'un groupe de discussion."
      description: ""
      parameters:
      - in: path
        name: channelName
        type: "string"
        required: true
        description: Nom du groupe de discussion dont on veut voir les messages.
      operationId: "getChannelMessages"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
            $ref: "#/definitions/Message"
        404:
          description: "Not found"
        500:
          description: "Error from server"    
  /messages/gif/{userName}:
    post:
      tags:
      - "Messages"
      summary: "Envoie un GIF au hasard pour un thème donné à un utilisateur."
      description: ""
      operationId: "sendRandomGifToUser"
      consumes:
      - "application/json"
      parameters:
      - in: path
        name: userName
        type: "string"
        required: true
        description: Nom de l'utilisateur à qui on veut envoyer un message.
      - in: "body"
        name: "body"
        description: "L'objet \"message\" à ajouter dans la base. La partie \"data\" sera le thème du GIF."
        required: true
        schema:
          $ref: "#/definitions/Message"
      responses:
        200:
          description: "Success"
        400:
          description: "Bad request"
        404:
          description: "User not found"
        500:
          description: "Error from server"
  /messages/gif/channel/{channelName}:
    post:
      tags:
      - "Messages"
      summary: "Envoie un GIF au hasard pour un thème donné à un groupe de discussion."
      description: ""
      operationId: "sendRandomGifToUser"
      consumes:
      - "application/json"
      parameters:
      - in: path
        name: channelName
        type: "string"
        required: true
        description: Nom du groupe de discussion où l'on veut poster un message.
      - in: "body"
        name: "body"
        description: "L'objet \"message\" à ajouter dans la base. La partie \"data\" sera le thème du GIF."
        required: true
        schema:
          $ref: "#/definitions/Message"
      responses:
        200:
          description: "Success"
        400:
          description: "Bad request"
        404:
          description: "Channel/user not found"
        500:
          description: "Error from server"
definitions:
  User:
    type: "object"
    properties:
      login :
        type : "string"
      password:
        type: "string"
  Channel :
    type: "object"
    properties:
      name :
        type : "string"
  Message :
    type : "object"
    properties :
      from :
        type : "string"
      pwd :
        type : "string"
      data :
        type : "string"
externalDocs:
  description: "Find out more about Swagger"
  url: "http://swagger.io"
