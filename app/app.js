const express			= require(`express`);
const app				= express();
const encrypt			= require(`mongoose-encryption`)
const bodyParser		= require(`body-parser`);
const swaggerUi 		= require('swagger-ui-express');
const YAML 				= require('yamljs');
const swaggerDocument 	= YAML.load('./swagger/swagger.yml');
const User          	= require(`./models/User`);
const Msg 				= require('./models/Msg');
const Channel 			= require('./models/Channel');
const request 		= require('request');
const giphyApiKey	= "seIZnfKsZEYaIAk9kEEG0rQnDren9JXd";
let mongoose 			= require('mongoose');
let uniqueValidator 	= require('mongoose-unique-validator');

mongoose.connect('mongodb://mongo:27017/base', {useNewUrlParser: true, useUnifiedTopology: true});
app.use(bodyParser.json());


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

console.log('Try to connect to DB...');
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  	console.log("We're connected to DB !");
});

				//USERS
//Crée un utilisateur (données : login + pwd)
app.post('/users',function(req,res){
    if (req.body.login && req.body.password) {
	    User.find({ login: req.body.login }, 'login', function (err, comms) {
	    	if (err){
	    		res.status(500).send("Server error" + err);
	   		}
	    	else{
	    		if(comms==""){
	    			var newUsr = new User ({ login : req.body.login, pwd : req.body.password });
	    			newUsr.save();
	    			res.status(200).send("User "+req.body.login+" was created ("+comms+")");
	    		}
	    		else
	    			res.status(400).send("User already in database");
	    	}
		});
	}
	else
		res.status(400).send("Incorrect data");
});

//Renvoie la liste de tous les utilisateurs
app.get('/users',function(req,res){
    User.find({}, 'login', function (err, comms) {
	    if (err) { 
	    	throw err; 
			res.status(500).send('Server error : ' + err);
	    }
	    else
	    	res.status(200).send(comms);   
	});
});

//Supprime un utilisateur (données : login + pwd)
app.delete('/users/:userName',function(req,res){
	if (req.params.userName) {
		User.findOne({ login : req.params.userName })
		.then(userDel => {
			if(userDel == null){
	    			res.status(404).send("User " + req.params.userName + "not found");
			}
			else {
		    		mongoose.set('useFindAndModify', false);
	    	    		User.findOneAndRemove({ login : req.params.userName }).exec();
			}
		});
	
		Msg.deleteMany({touser : req.params.userName},
		function(err, result) {
	      		if (err) {
	        		res.status(400).send("Error while deleting user's messages" + err);
	      		} else {
	        		res.status(200).send("Delete user and all messages ");
	      		}
	    	}
	  	);
	}
	else
		res.status(400).send("Incorrect data");
});

				//CHANNELS
//Crée un groupe de discussion (données : channelName)
app.post('/channels',function(req,res){
    if (req.body.name) {
	    Channel.find({ name: req.body.name }, 'name', function (err, comms) {
	    	if (err){
	    		res.status(500).send("Server error : " + err);
	   		}
	    	else{
	    		if(comms==""){
	    			var newChannel = new Channel ({ name : req.body.name });
	    			newChannel.save();
	    			res.status(200).send("Channel "+req.body.name+" was created ("+comms+")");
	    		}
	    		else
	    			res.status(400).send("Channel already in database");
	    	}
		});
	}
	else
		res.status(404).send("Incorrect data");
});

//Renvoie la liste de tous les groupes de discussion
app.get('/channels',function(req,res){
    Channel.find({}, 'name description', function (err, comms) {
	    if (err) { 
	    	throw err; 
			res.status(500).send('Error server : ' + err);
	    }
	    else
	    	res.status(200).send(comms);   
	});
});

//Supprime un groupe de discussion
app.delete('/channels/:channelName',function(req,res){
	if (req.params.channelName) {
	Channel.findOne({ name : req.params.channelName })
	    .then(channelDel => {
		if(channelDel == null){
	    	    res.status(404).send("Channel " + req.params.channelName + "not found");
		}
		else {
		    mongoose.set('useFindAndModify', false);
	    	    Channel.findOneAndRemove({ name : req.params.channelName }).exec();
		}
		
	    });
	Msg.deleteMany({tochan : req.params.channelName},
		function(err, result) {
	      		if (err) {
	        		res.status(400).send("Error while deleting messages" + err);
	      		} else {
	        		res.status(200).send("Delete channel and all messages ");
	      		}
	    	}
	  	);
	}
	else
		res.status(400).send("Incorrect data");
}); 

//MESSAGES
//Récupère les messages de l'utilisateur (données : login + pwd)
app.get('/messages/:userName',function(req,res){
	if (req.params.userName) {
			    		Msg.find({touser: req.params.userName}, 'from touser contenu updatedAt', function (err, comms){
			    			if (err) { 
						    	throw err; 
							res.status(500).send('Server error : ' + err);
						}
						else
							res.status(200).send(comms);   
			    		});
	}
	else
		res.status(400).send("Incorrect data");
}); 


//Récupère les messages d'un groupe de discussion
app.get('/messages/channel/:channelName',function(req,res){
	if(req.params.channelName){
    	Channel.findOne({name: req.params.channelName}, function (err, comms) {
			if(err)
				res.status(500).send("Server error : " + err);
			    if (comms == null)
			    	res.status(404).send("Channel " + req.params.channelName + "not found");
			    else{
			    		Msg.find({tochan: req.params.channelName}, 'from tochan contenu updatedAt', function (err, comms){
			    			if (err) { 
						    	throw err; 
								res.status(500).send('Server error : ' + err);
						    }
						    else
						    	res.status(200).send(comms);   
			    		});
			    	}
		});
    }
	else
		res.status(400).send("Incorrect data");
});

//Envoie un message à un utilisateur (données : login + pwd + mess)
app.post('/messages/:userName',function(req,res){
if (req.params.userName && req.body.from && req.body.data && req.body.pwd) {
	User.findOne({login: req.body.from, pwd: req.body.pwd }, function (err, comms) {
		if (err)
			res.status(500).send("Error server : " + err); 
		else{
		    if (comms == null)
		    	res.status(404).send("User " + req.body.from + " not found or incorrect password"); 
		    else{
		    		User.findOne({login: req.params.userName}, function (err, comms) {
		    		if(err)
		    			res.status(500).send("Error server : " + err); 
		    		else{
			    		if (comms == null)
			    			res.status(404).send("User "+req.params.userName+" not found");
			    		else{
			    				var newMsg = new Msg({from: req.body.from, touser: req.params.userName, contenu: req.body.data});
				    			newMsg.save();
				    			res.status(200).send("Message "+req.body.data+" was sent to "+req.params.userName);  
				    		}
				    	}
			    	});
			    }
			}
		});
	}
	else
		res.status(400).send("Incorrect data");
}); 

//Envoie un message à un groupe de discussion (données : login + pwd + mess)
app.post('/messages/channel/:channelName',function(req,res){
if (req.params.channelName && req.body.from && req.body.data && req.body.pwd) {
	User.findOne({login: req.body.from, pwd: req.body.pwd }, function (err, comms) {
		if (err)
			res.status(500).send("Server error : " + err); 
		else{
		    if (comms == null)
		    	res.status(404).send("User " + req.body.from + " not found or incorrect password"); 
		    else{
		    		Channel.findOne({name: req.params.channelName}, function (err, comms) {
		    		if(err)
		    			res.status(500).send("Server error : " + err); 
		    		else{
			    		if (comms == null)
			    			res.status(404).send("Channel "+req.params.channelName+" not found");
			    		else{
			    				var newMsg = new Msg({from: req.body.from, tochan: req.params.channelName, contenu: req.body.data});
				    			newMsg.save();
				    			res.status(200).send("Message "+req.body.data+" was sent to "+req.params.channelName);  
				    		}
				    	}
			    	});
			    }
			}
		});
	}
	else
		res.status(400).send("Incorrect data");
});  

//Envoie un GIF au hasard pour un thème donné à un utilisateur (données : login + pwd + mess)
app.post('/messages/gif/:userName',function(req,res){
if (req.params.userName && req.body.from && req.body.data && req.body.pwd) {
	User.findOne({login: req.body.from, pwd: req.body.pwd }, function (err, comms) {
		if (err)
			res.status(500).send("Server error : " + err); 
		else{
		    if (comms == null)
		    	res.status(404).send("User " + req.body.from + " not found or incorrect password"); 
		    else{
		    		User.findOne({login: req.params.userName}, function (err, comms) {
		    		if(err)
		    			res.status(500).send("Server error : " + err); 
		    		else{
			    		if (comms == null)
			    			res.status(404).send("User "+req.params.userName+" not found");
			    		else{
							var gifUrl;
							request("https://api.giphy.com/v1/gifs/random?api_key="+giphyApiKey+"&tag="+req.body.data+"&rating=G", { json: true }, (err, res, body) => {
  							if (err) { return console.log(err); }
							gifUrl= body.data.url;
			    				var newMsg = new Msg({from: req.body.from, touser: req.params.userName, contenu: gifUrl});
				    			newMsg.save();
							});
				    			res.status(200).send("GIF "+gifUrl+" was sent to "+req.params.userName);
				    		}
				    	}
			    	});
			    }
			}
		});
	}
	else
		res.status(400).send("Incorrect data");
}); 

//Envoie un GIF au hasard pour un thème donné à un groupe de discussion (données : login + pwd + mess)
app.post('/messages/gif/channel/:channelName',function(req,res){
if (req.params.channelName && req.body.from && req.body.data && req.body.pwd) {
	User.findOne({login: req.body.from, pwd: req.body.pwd }, function (err, comms) {
		if (err)
			res.status(500).send("Server error : " + err); 
		else{
		    if (comms == null)
		    	res.status(404).send("User " + req.body.from + " not found or incorrect password"); 
		    else{
		    		Channel.findOne({name: req.params.channelName}, function (err, comms) {
		    		if(err)
		    			res.status(500).send("Server error : " + err); 
		    		else{
			    		if (comms == null)
			    			res.status(404).send("Channel "+req.params.channelName+" not found");
			    		else{
							var gifUrl;
							request("https://api.giphy.com/v1/gifs/random?api_key="+giphyApiKey+"&tag="+req.body.data+"&rating=G", { json: true }, (err, res, body) => {
  							if (err) { return console.log(err); }
							gifUrl= body.data.url;
			    				var newMsg = new Msg({from: req.body.from, tochan: req.params.channelName, contenu: gifUrl});
				    			newMsg.save();
							});
				    			res.status(200).send("GIF "+gifUrl+" was sent to "+req.params.channelName);
				    		}
				    	}
			    	});
			    }
			}
		});
	}
	else
		res.status(400).send("Incorrect data");
}); 
	 



//Listen port
app.listen(8080, () => console.log(`Server listening on port 8080`));

//sendfile envoie fichier
