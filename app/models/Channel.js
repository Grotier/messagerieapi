const mongoose = require(`mongoose`)

let ChannelSchema = new mongoose.Schema({
	name: 		{ type: String, required: true },
  	description: 		{ type: String },
}, {timestamps: true});

module.exports = mongoose.model(`Channel`, ChannelSchema);