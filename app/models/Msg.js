let mongoose = require('mongoose');

let uniqueValidator = require('mongoose-unique-validator');
let crypto = require('crypto');

let MsgSchema = new mongoose.Schema({
	from: 			{ type: String, required: true },
	touser: 		String,
	tochan: 		String,
	contenu: 		{ type: String, required: true },
}, {timestamps: true});

module.exports = mongoose.model(`Msg`, MsgSchema);