let mongoose = require('mongoose');

let uniqueValidator = require('mongoose-unique-validator');
let crypto = require('crypto');

let UserSchema = new mongoose.Schema({
  	login: 			{ type: String, required: true },
  	nom: 			String, 
  	pwd: 			String,
  	prenom: 		String, 
  	email: 			{ type: String, match: [/\S+@\S+\.\S+/, 'is invalid'] },
  	bio: 			String,
  	hash: 			String,
}, {timestamps: true});
// cette dernière option permet de connaître la date de modif et d'ajout

UserSchema.plugin(uniqueValidator, {message: 'is already taken.'});

module.exports = mongoose.model(`User`, UserSchema);