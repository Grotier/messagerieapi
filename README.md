Created by Gautier Delacour and Bastien Duchesne in relation with Enseirb-Matmeca (2020).

# API de messagerie en NodeJS

## Users
+ Post /users -> créer un utilisateur.  ( {"name" : "", "password" : ""})
+ Get /users -> voir tous les utilisateurs existants.  
+ Delete /users/{userName} -> supprime un utilisateur. Supprime aussi les messages associés.

## Channels
+ Post /channels -> créer un channel. ( {"name" : ""} )  
+ Get /channels -> voir tous les channels existants.  
+ Delete /channels -> supprimer un channel. Supprime aussi les messages associés.  

## Messages
+ Post /messages/{userName} -> envoie un message à un user.  
+ Post /messages/channel/{channelName} -> envoie un message à un channel.  
+ Get /messages -> récupère les messages d'un utilisateurs ( {"name" : "", "password" : ""} )  
+ Get /messages/{channelName} -> récupère les messages d'un channel.  
+ Post /messages/gif/{userName} -> envoie un GIF à un user.  
+ Post /messages/gif/channel/{channelName} -> envoie un GIF à un channel.  

-----------------------------
# Installer et utiliser l'API

## Récupérer le dépôt Git.

Depuis un terminal :

	git clone https://gitlab.com/Grotier/messagerieapi.git

## Lancer l'environnement Docker depuis la racine du dépôt Git.

Depuis un terminal :

	docker-compose up -d --build

Pour être sûr d'être dans le bon dossier, vous devez trouver le fichier **docker-compose.yml** en tapant la command `ls`.

## Utiliser l'API.

Vous pouvez utiliser [Swagger](http://localhost:5002/api-docs/) ou des requêtes **curl** depuis votre terminal.

Pour les requêtes **curl**, le port visé doit être le **5002**.

*Exemple* :

	curl -X GET "http://localhost:5002/users" -H  "accept: application/json"
